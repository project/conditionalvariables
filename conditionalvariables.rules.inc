<?php

/**
 * Implementation of hook_rules_action_info().
 */
function conditionalvariables_rules_action_info() {
  $defaults = array(
   'parameter' => array(
      'variable' => array(
         'type' => 'list<text>',
         'label' => t('Variable'),
         'description' => t('The variable that should be changed.'),
         'options list' => 'conditionalvariables_var_list',
       ),
      'value' => array(
        'type' => 'text',
        'label' => t('Value'),
        'description' => t('The new value for the variable.'),
        'save' => TRUE,
      ),
    ),
    'group' => t('System'),
  );
  $items['conditionalvariables_change_variable'] = $defaults + array(
    'label' => t('Change Drupal variable'),
    'base' => 'conditionalvariables_change_variable',
  );

  return $items;
}

/**
 * Creates list of variables for choosing when configuring the rule.
 */
function conditionalvariables_var_list() {
  global $conf;
  foreach ($conf as $key => $value) {
    $values[$key] = $key;
  }
  return $values;
}


/**
 * Implements hook_rules_condition_info().
 */
function conditionalvariables_rules_condition_info() {
  $conditions = array();

  $conditions['conditionalvariables_page_condition'] = array(
    'label' => t('Page is being viewed'),
    'group' => t('System'),
    // Parameters are described identically to how they work for actions.
    'parameter' => array(
      'pages' => array(
        'type' => 'text',
        'label' => t('Page being viewed'),
        'description' => t('Specify pages by using their paths. Enter one path per line. The \'*\' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page.')
      ),
    ),
  );

  return $conditions;
}

/**
 * Support function for page condition which determines current page is within configured path settings.
 */
function conditionalvariables_page_condition($pages) {
  $pages = drupal_strtolower($pages);

  // Adapted from block.module block_block_list_alter() for visiblity settings entered into a textarea.

  // Convert the Drupal path to lowercase
  $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));
  // Compare the lowercase internal and lowercase path alias (if any).
  $page_match = drupal_match_path($path, $pages);
  if ($path != $_GET['q']) {
    $page_match = $page_match || drupal_match_path($_GET['q'], $pages);
  }
  
  return $page_match;
}